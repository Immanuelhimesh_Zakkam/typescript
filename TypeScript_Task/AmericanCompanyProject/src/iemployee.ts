export interface IEmployee {
  getCurrentProjectName(): string;
  getName(): string;
}
