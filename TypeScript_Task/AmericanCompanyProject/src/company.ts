import { IEmployee } from "./iemployee";
export class Company {
  private employeeList: IEmployee[] = [];

  public addEmployee(employee: IEmployee): IEmployee {
    this.employeeList.push(employee);
    return employee;
  }

  getListOfEmployeeProjects(): string[] {
    return this.employeeList.map((employee) => employee.getCurrentProjectName());
  }

  getNameList(): string[] {
    return this.employeeList.map((employee) => employee.getName());
  }
}
