import { Company } from "./company";
import { Frontend } from "./frontend";
import { Backend } from "./backend";

const europeanCompany = new Company();
const frontendEmployee = new Frontend("Sunil", ".Net");
const backendEmployee = new Backend("Neravati", "GoLang");
console.log(europeanCompany.addEmployee(frontendEmployee));
console.log(europeanCompany.addEmployee(backendEmployee));
console.log(europeanCompany.getListOfEmployeeProjects);
console.log(europeanCompany.getNameList);
