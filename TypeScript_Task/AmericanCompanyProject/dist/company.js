export class Company {
    constructor() {
        this.employeeList = [];
    }
    addEmployee(employee) {
        this.employeeList.push(employee);
        return employee;
    }
    getListOfEmployeeProjects() {
        return this.employeeList.map((employee) => employee.getCurrentProjectName());
    }
    getNameList() {
        return this.employeeList.map((employee) => employee.getName());
    }
}
