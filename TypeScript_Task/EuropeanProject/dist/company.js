export class Company {
    constructor() {
        this.employeesList = [];
    }
    add(employee) {
        this.employeesList.push(employee);
        return employee;
    }
    get getListOfEmployeeProjects() {
        return this.employeesList.map((employee) => employee.getCurrentProjectName());
    }
    get getNameList() {
        return this.employeesList.map((employee) => employee.getName());
    }
}
