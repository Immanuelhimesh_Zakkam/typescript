import { Employee } from "./employee";
export class Company {
  private employeesList: Employee[] = [];

  public add(employee: Employee): Employee {
    this.employeesList.push(employee);
    return employee;
  }

  get getListOfEmployeeProjects(): string[] {
    return this.employeesList.map((employee) => employee.getCurrentProjectName());
  }

  get getNameList(): string[] {
    return this.employeesList.map((employee) => employee.getName());
  }
}
