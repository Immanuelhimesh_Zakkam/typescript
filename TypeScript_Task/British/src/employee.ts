export class Employee {
  private name: string;
  private projectName: string;

  constructor(name: string, project: string) {
    this.name = name;
    this.projectName = project;
  }

  get getCurrentProject(): string {
    return this.projectName;
  }

  get getName(): string {
    return this.name;
  }
}
