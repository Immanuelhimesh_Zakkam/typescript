import { CompanyLocationLocalStorage } from "./companyLocalStorage";
import { Employee } from "./employee";
import { ILocation } from "./ilocation";

export class Company {
  private location: ILocation;
  private employeeList: Employee[] = [];

  constructor(location: ILocation) {
    this.location = location;
  }

  addEmployee(employee: Employee): void {
    this.employeeList.push(employee);
    this.location.addPerson(employee);
  }

  getProjectList(): string[] {
    if (this.location instanceof CompanyLocationLocalStorage) {
      return this.location
        .getPersons()
        .map((employee) => employee.getCurrentProject);
    }
    return this.employeeList.map((employee) => employee.getCurrentProject);
  }

  getNameList(): string[] {
    if (this.location instanceof CompanyLocationLocalStorage) {
      return this.location.getPersons().map((employee) => employee.getName);
    }
    return this.employeeList.map((employee) => employee.getName);
  }

  getEmployeeCount(): number {
    return this.location.getCount();
  }
}
