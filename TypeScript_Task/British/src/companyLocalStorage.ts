import { Employee } from "./employee";
import { ILocation } from "./ilocation";

export class CompanyLocationLocalStorage implements ILocation {
  private localStorageKey: string;
  private personList: Employee[];

  constructor(localStorageKey: string) {
    this.localStorageKey = localStorageKey;
    this.personList = JSON.parse(
      localStorage.getItem(this.localStorageKey) || "[]",
      (key, value) => {
        if (key === "" && Array.isArray(value)) {
          return value.map(
            (employeeData) =>
              new Employee(employeeData.name, employeeData.project)
          );
        }

        return value;
      }
    );
  }

  addPerson(person: Employee): void {
    this.personList.push(person);
    localStorage.setItem(this.localStorageKey, JSON.stringify(this.personList));
  }

  getPerson(index: number): Employee | null {
    return this.personList[index] || null;
  }

  getCount(): number {
    return this.personList.length;
  }

  getPersons(): Employee[] {
    return this.personList;
  }
}
