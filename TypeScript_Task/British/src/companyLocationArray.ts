import { Employee } from "./employee";
import { ILocation } from "./ilocation";

export class CompanyLocationArray implements ILocation {
  private personList: Employee[] = [];

  addPerson(person: Employee): void {
    this.personList.push(person);
  }

  getPerson(index: number): Employee | null {
    return this.personList[index] || null;
  }

  getCount(): number {
    return this.personList.length;
  }

  getPersons(): Employee[] {
    return this.personList;
  }
}
