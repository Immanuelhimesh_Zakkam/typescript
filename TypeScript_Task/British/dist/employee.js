export class Employee {
    constructor(name, project) {
        this.name = name;
        this.projectName = project;
    }
    get getCurrentProject() {
        return this.projectName;
    }
    get getName() {
        return this.name;
    }
}
