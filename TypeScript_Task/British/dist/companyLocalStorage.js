import { Employee } from "./employee.js";
export class CompanyLocationLocalStorage {
    constructor(localStorageKey) {
        this.localStorageKey = localStorageKey;
        this.personList = JSON.parse(localStorage.getItem(this.localStorageKey) || "[]", (key, value) => {
            if (key === "" && Array.isArray(value)) {
                return value.map((employeeData) => new Employee(employeeData.name, employeeData.project));
            }
            return value;
        });
    }
    addPerson(person) {
        this.personList.push(person);
        localStorage.setItem(this.localStorageKey, JSON.stringify(this.personList));
    }
    getPerson(index) {
        return this.personList[index] || null;
    }
    getCount() {
        return this.personList.length;
    }
    getPersons() {
        return this.personList;
    }
}
