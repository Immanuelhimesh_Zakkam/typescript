export class CompanyLocationArray {
    constructor() {
        this.personList = [];
    }
    addPerson(person) {
        this.personList.push(person);
    }
    getPerson(index) {
        return this.personList[index] || null;
    }
    getCount() {
        return this.personList.length;
    }
    getPersons() {
        return this.personList;
    }
}
