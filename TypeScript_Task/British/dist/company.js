import { CompanyLocationLocalStorage } from "./companyLocalStorage.js";
export class Company {
    constructor(location) {
        this.employeeList = [];
        this.location = location;
    }
    addEmployee(employee) {
        this.employeeList.push(employee);
        this.location.addPerson(employee);
    }
    getProjectList() {
        if (this.location instanceof CompanyLocationLocalStorage) {
            return this.location
                .getPersons()
                .map((employee) => employee.getCurrentProject);
        }
        return this.employeeList.map((employee) => employee.getCurrentProject);
    }
    getNameList() {
        if (this.location instanceof CompanyLocationLocalStorage) {
            return this.location.getPersons().map((employee) => employee.getName);
        }
        return this.employeeList.map((employee) => employee.getName);
    }
    getEmployeeCount() {
        return this.location.getCount();
    }
}
